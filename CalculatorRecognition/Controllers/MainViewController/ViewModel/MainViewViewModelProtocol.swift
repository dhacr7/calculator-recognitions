//
//  MainViewViewModelProtocol.swift
//  CalculatorRecognition
//
//  Created by User on 05/05/23.
//

import Foundation
import UIKit

protocol MainViewViewModelProtocol {
  var text: String { get }
  var themeColor: UIColor { get }
  var flavour: Flavour { get }

  var result: Published<Int>.Publisher { get }

  func readImage(
    image: UIImage,
    onCompletion: @escaping DoubleResult<Bool, String?>
  )
}
