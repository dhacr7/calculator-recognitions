//
//  ConfigProvider.swift
//  CalculatorRecognition
//
//  Created by User on 05/05/23.
//

import Foundation

enum ConfigKey: String {
  case appTheme = "AppTheme"
  case appFlavour = "AppFlavour"
}

private func bundleOption(_ key: String) -> Any? {
  return Bundle.main.object(forInfoDictionaryKey: key)
}

private func stringBundleOption(key: ConfigKey) -> String? {
  return bundleOption(key.rawValue) as? String
}

struct ConfigProvider {
  static let appTheme: String = {
    stringBundleOption(key: .appTheme) ?? ""
  }()

  static let appFlavour: String = {
    stringBundleOption(key: .appFlavour) ?? ""
  }()
}
