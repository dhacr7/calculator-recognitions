//
//  Bundle.swift
//  CalculatorRecognition
//
//  Created by User on 05/05/23.
//

import Foundation

extension Bundle {
  var bundleID: String {
    return infoDictionary?["CFBundleIdentifier"] as? String ?? ""
  }

  var appName: String {
    return infoDictionary?["CFBundleName"] as? String ?? ""
  }
}
