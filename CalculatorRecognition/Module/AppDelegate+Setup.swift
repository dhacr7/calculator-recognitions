//
//  AppDelegate+Setup.swift
//  CalculatorRecognition
//
//  Created by User on 05/05/23.
//

import Foundation
import IQKeyboardManagerSwift

extension AppDelegate {
  func setup() {
    IQKeyboardManager.shared.enable = true
    IQKeyboardManager.shared.shouldResignOnTouchOutside = true
  }
}
