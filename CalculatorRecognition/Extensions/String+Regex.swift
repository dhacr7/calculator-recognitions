//
//  String+Regex.swift
//  CalculatorRecognition
//
//  Created by User on 05/05/23.
//

import Foundation

extension String {
  func matches(for regex: String) -> [String] {
    do {
      let regex = try NSRegularExpression(pattern: regex)
      let results = regex.matches(
        in: self,
        range: NSRange(startIndex..., in: self)
      )
      return results.map {
        String(self[Range($0.range, in: self)!])
      }
    } catch let error {
      print("invalid regex: \(error.localizedDescription)")
      return []
    }
  }
}
